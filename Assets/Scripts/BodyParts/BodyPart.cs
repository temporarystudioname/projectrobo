using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPart : MonoBehaviour
{
    // Start is called before the first frame update
    public List<float> _stats; 
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// This function should specify the special jump behavior of the bodypart influencing the pawns y movement
    /// </summary>
    public void Jump()
    {
        
    }

    /// <summary>
    /// This function specifies the attack behavior of our body part
    /// </summary>
    public void Attack()
    {

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arms : BodyPart
{
    private float _attackSpeed;
    private float _damage;
    private float _range;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// This function defines the special attack behavior of this weapon
    /// </summary>
    new public void Attack()
    {

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private float _health = 100;
    [SerializeField]
    private float _groundSpeed = 10;
    [SerializeField]
    private float _airSpeed = 10;
    [SerializeField]
    private float _jumpSpeed = 2;
    [SerializeField]
    private int _maxJumps = 2;
    [SerializeField]
    private float _mass = 100;

    private int _currentJumps;

    private BodyPart _leftArm;
    private BodyPart _rightArm;
    private BodyPart _body;
    private BodyPart _back;
    private BodyPart _head;
    private BodyPart _legs;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// This function should make any pawn move in x,z direction on the ground dependent on the stats of his equipped legs
    /// </summary>
    /// <param name="direction">
    /// 0 < x,z < 1, y = 0 </param>
    public void Move(Vector3 direction)
    {
        
    }

    /// <summary>
    /// This function should let the pawn jump dependent on his equipped legs
    /// </summary>
    public void Jump()
    {

    }

    /// <summary>
    /// This function should deal damage to the pawn
    /// </summary>
    /// <param name="damage">
    /// value defining how much damage </param>
    public void TakeDamage(float damage)
    {
        
    }

    /// <summary>
    /// This function should give the pawn health
    /// </summary>
    /// <param name="health">
    /// value defining how much health should be gained </param>
    public void AddHealth(float health)
    {

    }

}
